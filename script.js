function createNewUser() {
    const newUser = {};

    newUser.firstName = prompt("Введіть Ваше ім'я:");
    newUser.lastName = prompt("Введіть Ваше прізвище:");
    newUser.birthday = prompt("Введіть Вашу дату народження (у форматі dd.mm.yyyy):");

    newUser.getLogin = function() {
        const firstLetter = this.firstName.charAt(0).toLowerCase();
        return firstLetter + this.lastName.toLowerCase();
    };

    newUser.getAge = function() {
        const birthYear = this.birthday.slice(-4);
        const currentYear = new Date().getFullYear();
        const age = currentYear - parseInt(birthYear);

        const birthDate = new Date(this.birthday.split('.').reverse().join('-'));
        const birthYearThisYear = new Date(currentYear, birthDate.getMonth(), birthDate.getDate());
        if (new Date() < birthYearThisYear) {
            return age - 1;
        }
        return age;
    };

    newUser.getPassword = function() {
        return `${newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.slice(-4)}`;
    }

    return newUser;
}

const user = createNewUser();
console.log(user);
console.log (user.getAge());
console.log(user.getPassword());
